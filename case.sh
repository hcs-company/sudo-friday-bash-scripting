#!/bin/bash

DEBUG=false
VERBOSE=false

while [ "${#}" -gt 0 ]; do
  case "${1}" in
    --debug|-d)
      DEBUG=true
      ;;
    --verbose|-v)
      VERBOSE=true
      ;;
    --subject|-s)
      SUBJECT=${2}
      shift
  esac
  shift
done

echo "DEBUG is ${DEBUG}"
echo "VERBOSE is ${VERBOSE}"
echo "SUBJECT is ${SUBJECT:-World}"

#!/bin/bash

TO_CHECK="${1:-FOO}"

echo "Simpele if"
if true; then
  echo "True"
else
  echo "False"
fi

echo

echo "Enkele haak ([) / test"
if [ "${TO_CHECK}" == "FOO" ]; then
  echo "FOO Found"
else
  echo "FOO not found"
fi

echo

echo "Dubbele haak ([[) met globbing"
if [[ ${TO_CHECK} == FO* ]]; then
  echo "FO* Found"
else
  echo "FO* not found"
fi

echo

echo "Dubbele haak met regex"
if [[ ${TO_CHECK} =~ ^FO.{1,2}$ ]]; then
  echo "FO. Found"
else
  echo "FO.{1,2} not found"
fi

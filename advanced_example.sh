#!/bin/bash
FILEPATH=./song.txt
CONTENTS="$(cat $FILEPATH)"
COLOR='\033[1;31m'
NOCOLOR='\033[0m'

removeSpaces() {
local COLOR='\033[1;31m'

echo -e "${COLOR}Spaties Verwijderd${NOCOLOR}\n"
echo "$CONTENTS" | sed "s/ //g"
}

printText() {
local COLOR='\033[0;32m'
	
echo -e "${COLOR}Niks gedaan met de text${NOCOLOR}\n"
echo "$CONTENTS" 
}

case $1 in
  removespaces)
    removeSpaces "{$CONTENTS}"
  ;;
  *)
    printText "{$CONTENTS}"
esac

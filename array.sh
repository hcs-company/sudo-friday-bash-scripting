#!/bin/bash

DEBUG=false
VERBOSE=false
SUBJECTS=()

while [ "${#}" -gt 0 ]; do
  case "${1}" in
    --debug|-d)
      DEBUG=true
      ;;
    --verbose|-v)
      VERBOSE=true
      ;;
    --subject|-s)
      SUBJECTS+=("${2}")
      shift
  esac
  shift
done

echo "DEBUG is ${DEBUG}"
echo "VERBOSE is ${VERBOSE}"

for SUB in "${SUBJECTS[@]}"; do
  echo "Hallo ${SUB}, hoe gaat het met jou?"
done


# Sudo Friday Bash Scripting

In dit repo staan de voorbeelden die tijdens [Sudo
Friday](https://www.youtube.com/channel/UCpdcubZ5zk-fuAR03AInL6g) Bash
Scripting serie zijn gemaakt.

De code voor de verschillende voorbeelden staat hier. Als er afleveringen
bijkomen zal de bijbehorende code hier ook verschijnen.
